function contains(full, sub) {
    return full.indexOf(sub)>-1;
}

function logURL(req) {
    const url = req.url;

    let turl = req.originUrl;
    if (req.frameAncestors.length) {
        turl = req.frameAncestors[0].url;
    }

    let title, ext;
    if (contains(turl, 'kissasian.sh/Drama')) {
        // === FE server, UNKNOWN
        if (contains(url, 'hlsmp4.com')
            || contains(url, 'playercdn.net')
            || (contains(url, 'video.xx.fbcdn.net') && contains(turl, 's=fb'))) {
            title =  turl.split('Drama/')[1].split('?')[0].replace('/', '_');

            const parts = url.split('?')[0].split('.');
            ext = parts[parts.length - 1];
        }
    }

    if (contains(turl, 'kseries.me/watch')
            && (contains(url, 'googlevideo.com/videoplayback'))) {
        title = turl.split('kseries.me/watch/')[1]
            .replace('/', '').replace('-episode-', '_');
        ext = url.split('mime=video/')[1].split('&')[0];
    }

    dlFile(url, title, ext);
}

const allFiles = [];
function dlFile(url, title, ext) {
    const fname = title + '.' + ext;

    if (allFiles.indexOf(fname)==-1 && title && ext) {
        browser.downloads.download({
            url, filename: 'viu/'+fname
        });
        allFiles.push(fname);
    }
}

browser.webRequest.onBeforeRequest.addListener(
        logURL, {urls: ["<all_urls>"]});
