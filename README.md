# Script - automated web stream downloader

This example uses the firefox browser functions exposed to extensions to automatically download videos from streaming sites. For this particular example, I have used kissasian, which is one of the very popular streaming sites out there.

To set it up for the streaming website you use, you will first have to figure out what the url to the video is.

I wont be covering much of that here, but in general, you will be doing a lot of digging around using the browser console tools.

Once you know where the link for the video is, this script will help you automate the whole process of checking for the video links and downloading it.

The script is setup in such a way that whenever the video is loaded from a certain site, once it passes all the checks, the video will be downloaded immediately.

**disclaimer:** This is a firefox extension that uses `browser.downloads` and `browser.webRequest` - just a POC to show that automating this kind of tasks works. It might be extended in the future, or not, depending on whether I have the time and need for it.
